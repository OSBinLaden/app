/**
 * 创建iframe
 * @param {Object} el
 * @param {Object} opt
 */
var createIframe = function(el, opt) {
	var elContainer = document.querySelector(el);
	var wrapper = document.querySelector(".mui-iframe-wrapper");
	if (!wrapper) {
		// 创建wrapper 和 iframe
		wrapper = document.createElement('div');
		wrapper.className = 'mui-iframe-wrapper';
		for (var i in opt.style) {
			wrapper.style[i] = opt.style[i];
		}
		var iframe = document.createElement('iframe');
		iframe.src = opt.url;
		iframe.id = opt.id || opt.url;
		iframe.name = opt.id;
		wrapper.appendChild(iframe);
		elContainer.appendChild(wrapper);
	} else {
		var iframe = wrapper.querySelector('iframe');
		iframe.src = opt.url;
		iframe.id = opt.id || opt.url;
		iframe.name = iframe.id;
	}
}

var contextPath = "http://dd.hch5.cc";
var tokenId = ""; //登录tockenId


var openDefaultPage = function(url) {
	mui.openWindow({
		url: url,
		id: url + "_" + Math.random(),
		createNew: false,
		show: {
//              autoShow: false,
                aniShow:'fade-in'
            },
		waiting: {
			autoShow: true,
			title: '正在加载...',
		}
	})
}

var errorCallBackFn = function(xhr, textStatus, errorThrown) {
	// 401 删除登录信息并跳转到登录页
	if (xhr.status == '401') {
		mui.alert('用户已失效!', function() {
				if(plus && plus.storage){
					plus.storage.removeItem("tokenId");
				}
				 openDefaultPage("weixinLogin.html");
			});
		
	} else {
		var error = "";
		if(xhr){
			error += JSON.stringify(xhr) ;
		}
		if(textStatus){
			error += JSON.stringify(textStatus) ;
		}
		mui.alert("异常"+error);
		
		
	}

}

//Token失效统一处理
var validResultCode = function(code) {
	if ("4001" == code) {
		openDefaultPage("weixinLogin.html");
	}
}



mui('.mui-bar-nav').on('tap', '.mui-icon-home ', function() {
	var url = this.getAttribute("href");
	openDefaultPage("index.html"); //对应的页面
});

mui('.bar-footer').on('tap', '.mui-tab-item', function() {
	var url = this.getAttribute("href");
	if (url != "#") {
		openDefaultPage(url); //对应的页面
	}
});
